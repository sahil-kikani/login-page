import React, { useEffect, useRef, useState, useCallback } from 'react'
import Home from '../components/Home'
import '../components/Registration.css'
function Registrattion () {
  // useRef hook
  const name = useRef()
  const city = useRef()
  const email = useRef()
  const password = useRef()

  // useState hook
  const [showHome, setShowHome] = useState(false)
  const [show, setShow] = useState(false)
  const localSignUp = localStorage.getItem('signUp')
  const localEmail = localStorage.getItem('email')
  const localPassword = localStorage.getItem('password')
  const localName = localStorage.getItem('name')

  // useEffect hook
  useEffect(() => {
    if (localSignUp) {
      setShowHome(true)
    }
    if (localEmail) {
      setShow(true)
    }
  })
  const handleClick = (e) => {
    e.preventDefault()
    if (
      name.current.value &&
      city.current.value &&
      email.current.value &&
      password.current.value
    ) {
      localStorage.setItem('name', name.current.value)
      localStorage.setItem('city', city.current.value)
      localStorage.setItem('email', email.current.value)
      localStorage.setItem('password', password.current.value)
      localStorage.setItem('signUp', email.current.value)
      alert('Account created successfully!!')
      window.location.reload()
    } else {
      alert('please enter valid input / Password must be grater than 5 character ')
    }
  }

  const handleSignIn = useCallback(
    (e) => {
      e.preventDefault()
      if (
        email.current.value === localEmail &&
        password.current.value === localPassword
      ) {
        localStorage.setItem('signUp', email.current.value)
        window.location.reload()
      } else {
        alert('Please Enter valid Credential')
      }
    }
  )
  return (
    <div>
      {showHome
        ? (
        <Home />
          )
        : show
          ? (
        <div className="container">
          <h3>
            Hello {localName}
          </h3>
          <div className="input_space">
            <input placeholder="Email" type="text" ref={email} />
          </div>
          <div className="input_space">
            <input placeholder="Password" type="password" ref={password} />
          </div>
          <button className="btn" onClick={(e) => handleSignIn(e)}>Sign In</button>
        </div>
            )
          : (
        <div className="container">
          <h1>Registration Page</h1>
          <br />
          <div className="input_space">
            <input placeholder="Name" type="text" ref={name} />
          </div>
          <div className="input_space">
            <input placeholder="Enter city" type="text" ref={city} />
          </div>
          <div className="input_space">
            <input placeholder="Email" type="text" ref={email} />
          </div>
          <div className="input_space">
            <input placeholder="Password" type="password" ref={password} />
          </div>
          <button className="btn" onClick={(e) => handleClick(e)}>Sign Up</button>
        </div>
            )}
    </div>
  )
}
export default Registrattion
